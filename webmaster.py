#This file will hold the helper functions to allow the scavenger app to handle the
#generic features of fetching links and parsing posts.
from bs4 import BeautifulSoup
import asyncio
import aiohttp
import sqlite3
import datetime
import smtplib

class WebMaster():

    def __init__(self, includes, excludes, db_location, email_config):
        self.include_list = includes
        self.exclude_list = excludes
        self.db_location = db_location
        self.db_conn = sqlite3.connect(self.db_location)
        self.db_cursor = self.db_conn.cursor()
        self.email = email_config


    #This design was reccomended by some helpful members on #python (freenode)
    #thank you \u03b5 and whg
    @asyncio.coroutine
    def start_fetch(self, sites):
        posts = []

        for site in sites:
            url = sites[site]['domain'] + sites[site]['path']
            links = yield from self.fetch_links(url, sites[site]['link_class'])
            site_info = sites[site]
            site_info['name'] = site
            new_links = self.db_filter(links, sites[site]['domain'])

            for link in new_links:
                post = yield from self.build_post(link, site_info)
                posts.append(post)

        if posts:
            self.insert_posts(posts)
            self.email_posts(posts)

        print("Number of new posts: " + str(len(posts)))

        return len(posts)

    #build post dict out of link and other data
    @asyncio.coroutine
    def build_post(self, link, site):
        href = site['domain'] + link['href']
        short = yield from self.shorten_url(href)
        page = yield from self._GET(href)
        content = self.parse_post(page, site['post_class'])
        now = datetime.datetime.now().isoformat(' ')
        return {"url": href, "site": site['name'], "title":  str(link.string), "content": str(content), "short": short, "time_stamp": now}




    #Send myself a message with the titles paired with the shortend links
    def email_posts(self, posts):
        body = "<h4>Recent Links<h4><br />"

        body += self._build_table(posts)

        subject = str(len(posts)) + " New Links Posted"
        self.send_mail(subject, body)




    #send mail
    def send_mail(self, subject, body):
        headers = [
            "From: <" + self.email["from"] + ">",
            "To: <" + self.email["to"] + ">",
            "Subject: " + subject,
            "Content-Type: text/html"
        ]

        session = smtplib.SMTP(self.email['host'], self.email['port'])
        session.ehlo()
        session.starttls()
        session.ehlo()

        session.login(self.email['user'], self.email['pass'])

        session.sendmail(self.email["from"], self.email["to"], "\r\n".join(headers) + "\r\n\r\n" + body)
        session.quit()


    #Need to build an html table of links
    def _build_table(self, posts):
        tbl = "<table><thead><tr><th>Title</th><th>Link</th></tr></thead><tbody>"
        for post in posts:
            tbl += "<tr><td>" + post['title'] + "</td><td>" + post['short'] + "</td></tr>"

        tbl += "</tbody></table>"

        return tbl

    #Take a set of links, and check to see if each URL is in the DB
    #return a set of links that have been filtered
    def db_filter(self, links, domain):
        hrefs = [domain + l['href'] for l in links]
        url_params = ','.join('?'*len(hrefs))
        qry = 'SELECT url FROM posts WHERE url in (' + url_params + ')'
        rows = self.db_cursor.execute(qry, hrefs).fetchall()

        url_set = {url[0] for url in rows}

        return [l for l in links if (domain + l['href']) not in url_set]



    #We need to store each page and its data in the DB
    def insert_posts(self, posts):
        inserts = [[post['url'], post['site'], post['title'], post['content'], post['short'], post['time_stamp']] for post in posts]
        self.db_cursor.executemany('INSERT INTO posts VALUES (?,?,?,?,?,?)', inserts)
        self.db_conn.commit()



    #Parse the page content out of the page
    def parse_post(self, page, post_class):
        bs_page = BeautifulSoup(page)
        post = bs_page.find("div", class_=post_class)
        post_dict = {}

        return post


    #Create a Short link from the link post
    @asyncio.coroutine
    def shorten_url(self, url):
        isgd_url = "http://is.gd/create.php?format=simple&url=" + url
        page = yield from self._GET(isgd_url, compress='True')
        return page.decode("utf-8")


    #Fetch the links from the right URL
    #Look into how to modularize this method with specific sub methods for grepping links on
    #the right pages.
    @asyncio.coroutine
    def fetch_links(self, url, link_class):

        #The yield from is required to call the @async.coroutine methods
        page = yield from self._GET(url)
        bs_page = BeautifulSoup(page)
        links = bs_page.find_all("a", class_=link_class)

        filtered = self.filter_links(links)

        return filtered

    #Filter out links based on include/exclude lists
    def filter_links(self, links):
        keep = []
        for link in links:
            title = str(link.string).lower()
            if self._filter_excludes(title) and self._filter_includes(title):
                keep.append(link)
        return keep


    #Take a BS4 tag, and returns true if it should not be excluded.
    #Returns false if a term in the exclude list is found
    def _filter_excludes(self, link):
        for exclude in self.exclude_list:
            if exclude in link:
                return False

        return True


    #Take a BS4 tag, and return true if it should be included
    #Returns false if an include term is not found
    def _filter_includes(self, link):
        for include in self.include_list:
            if include in link:
                return True

        return False


    #Async routine to run HTTP GET method on specifc URL
    @asyncio.coroutine
    def _GET(self, *args, **kwargs):
        response = yield from aiohttp.request('GET', *args, **kwargs)
        return (yield from response.read_and_close())
