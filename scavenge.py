#@author stanner
#This is an async web scraper. It uses modules for indivudal sites to handle descrepencies
#between the way they are organized and accessed.


#imports
import argparse
import json
import asyncio
from webmaster import WebMaster
import time

#setup argument parsing
parser = argparse.ArgumentParser(description='Get notifications when search terms appear on certain forums.')
parser.add_argument('--config', help='Location of the config file.')


def main():
    cmdl_args = parser.parse_args()
    config_file = cmdl_args.config

    config = {}

    with open(config_file, 'r') as cf:
        text = cf.read()
        config = json.loads(text)

    loop = asyncio.get_event_loop()
    webmaster = WebMaster(config["includes"], config["excludes"], config["database"], config['email'])
    while True:
        # try:

        loop.run_until_complete(webmaster.start_fetch(config["sites"]))
        # except Exception as inst:
            # print(inst)
        # finally:
        print("Sleeping: " + str(config['sleep']/60) + " minutes")
        time.sleep(config["sleep"])
        #close the event loop
        

    loop.close()


if __name__ == "__main__":
    # execute only if run as a script
    main()
